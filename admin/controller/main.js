tatLoading();
idProductUpdate=null;
function fetchProduct(){
  batLoading();                                             
  productServ
  .getList()
  .then(function(res){
  renderProduct(res.data);
  tatLoading();
  })
  .catch(function(err){
    tatLoading();
  }
  )
}
fetchProduct();
function themSP(){

  batLoading();
 var spanElements = document.querySelectorAll(".sp-thongbao");
  spanElements.forEach(function(spanElement) {
    spanElement.style.display = "block";
  });
    var newProduct=layThongTinTuForm();
    var isValid=checkNumber("tbPrice","number only! & >1000$",newProduct.price)&checkName("tbName","can't contain whitespace!",newProduct.name)&checkType("tbType","Samsung and Iphone only!",newProduct.type)&checNoneSpace("tbImg","Can't contain whitespace",newProduct.img)&checkNull("tbScreen","can't be empty",newProduct.screen)&checkNull("tbBCamera","can't be empty",newProduct.backCamera)&checkNull("tbFCamera","can't be empty",newProduct.frontCamera)&checkNull("tbDesc","can't be empty",newProduct.desc);
    if(isValid){
      productServ
  .create(newProduct)
  .then(function(res){
    fetchProduct();
    document.getElementById("formBody").reset();
    $("#exampleModal").modal("hide");
    tatLoading();
    
  }
  )
  .catch(function(err){

    tatLoading();

  });
  }else{
        tatLoading();
  }}


  function deleteSP(id){
    batLoading();
    productServ
    .delete(id)
    .then(function(res){
      fetchProduct();
      renderProduct(res.data);
      tatLoading();
    })
    .catch(function(err){
    
     tatLoading();

    })
    
  }
  function showInformation(id){
batLoading();
    idProductUpdate=id;
    productServ
    .getID(id)
    .then(function(res){
      showThongTinLenForm(res.data);
      tatLoading();
    }
    ).catch(function(err){
     tatLoading();
    })
  
  }
 function capNhatSP(){
   batLoading();
   var spanElements = document.querySelectorAll(".sp-thongbao");
   spanElements.forEach(function(spanElement) {
     spanElement.style.display = "block";
   });
   var product=layThongTinTuForm();
   var isValid=checkNumber("tbPrice","number only! & >1000$",product.price)&checkName("tbName","Can't contain whitespace!",product.name)&checkType("tbType","Samsung and Iphone only!",product.type)&checNoneSpace("tbImg","Can't contain whitespace",product.img)&checkNull("tbScreen","can't be empty",product.screen)&checkNull("tbBCamera","can't be empty",product.backCamera)&checkNull("tbFCamera","can't be empty",product.frontCamera)&checkNull("tbDesc","can't be empty",product.desc);
   if(isValid){
     productServ
     .update(idProductUpdate,product)
     .then(function(res){
       tatLoading();
       fetchProduct();
       $("#exampleModal").modal("hide");
  }
 ).catch(function(err){
tatLoading();
 });}
 else{
  tatLoading();
 }
  }

  function searchSP(){
  batLoading();  
  productServ
  .getList()
  .then(function(res){
  renderResult(res.data);
  tatLoading();
  }).catch(function(err){  
    tatLoading();  
  }
  )

  }
  function sortAcending(){
  batLoading();  
  productServ
  .getList()
  .then(function(res){
  renderAcending(res.data);
  tatLoading();
  }).catch(function(err){  
    tatLoading();  
  }
  )

  }
  function sortDecending(){
  batLoading();  
  productServ
  .getList()
  .then(function(res){
  renderDecending(res.data);
  tatLoading();
  }).catch(function(err){  
    tatLoading();  
  }
  )
  }
  function clearSpan(){
   var spanElements = document.querySelectorAll(".sp-thongbao");
  spanElements.forEach(function(spanElement) {
    spanElement.style.display = "none";
  });
  }

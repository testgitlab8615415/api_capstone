function showMessage(spanID, message){
    document.getElementById(spanID).innerHTML=message;
    }
    function checkNumber(spanID,message, number){
        const regex=/^\d+$/;
        var re= regex.test(number);
        if(re && number>=1000){
            showMessage(spanID,"");
            return true;
        }else{
            showMessage(spanID,message);
            return false;
        }
    }
    function checkName(spanID,message,name){
        const regex=/^.+$/;
        var re= regex.test(name);
        if(re){
            showMessage(spanID,"");
            return true;
        }else{
            showMessage(spanID,message);
            return false;
        }
    }
    function checkType(spanID,message,type){
    if(type=="Samsung" || type=="Iphone") {
     showMessage(spanID,"");
    return true;
    }else{
    showMessage(spanID,message);
    return false;
    }
    }
    
    function checNoneSpace(spanID, message, text){
        const regex= /^\S*$/
        const regex2= /^.+$/;
        var re=regex.test(text);
        var re2=regex2.test(text);
        if(re && re2){
            showMessage(spanID,"");
            return true;
        }else{
            showMessage(spanID,message);
            return false;
        }
    
    }
    function checkNull(spanID, message, text){
        const regex= /^.+$/;
        var re=regex.test(text);
        if(re){
            showMessage(spanID,"");
            return true;
        }else{
            showMessage(spanID,message);
            return false;
        }
    
    }
    